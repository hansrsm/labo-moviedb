

document.addEventListener("DOMContentLoaded", function(){


    let connexion = new MovieDb();

    console.log(document.location);

    if(document.location.pathname.search("fiche-film.html") > 0){

        let params = ( new URL(document.location) ).searchParams;

        console.log(params.get("id"));

        connexion.requeteInfoFilm(params.get("id"));

        connexion.requeteInfoActeur(params.get("id"));

    }

    else{

        connexion.requeteTopRated();



    }


});



class MovieDb{

    // constructor appellée en premier

    constructor(){

        this.APIkey = "399fc268c62e5639c50263903b826097";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 8;

        this.totalActeur = 6;

    }

    requeteTopRated(){

        let xhr = new XMLHttpRequest();
        // xhr.withCredentials = true;

    xhr.addEventListener("readystatechange", this.retourRequeteTopRated.bind(this) );


    xhr.open("GET", this.baseURL + "movie/top_rated?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

    xhr.send();
}

    retourRequeteTopRated(e){

        let target = e.currentTarget; //XMLHttpRequest

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            this.afficheTopRated(data);

            // console.log(data[2].title);
            // console.log(data[2].release_date);
        }

    }

    afficheTopRated(data){

        for(let i=0; i< this.totalFilm; i++){

            console.log(data[i]);

            let unArticle = document.querySelector(".template>article.film").cloneNode(true);

            unArticle.querySelector("h2").innerText = data[i].title;

            unArticle.querySelector("a").href = "fiche-film.html?id=" + data[i].id;

            if(data[i].overview == ""){

                unArticle.querySelector("p.description").innerText = "Description à venir"

            }
            else{

                unArticle.querySelector("p.description").innerText = data[i].overview;

            }

             let uneImage = unArticle.querySelector("img").setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);



            document.querySelector(".liste-films").appendChild(unArticle);

        }


    }



    requeteInfoFilm(idFilm){

        var xhr = new XMLHttpRequest();
        // xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourRequeteInfoFilm.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + idFilm + "?language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();

    }

    retourRequeteInfoFilm(e){

        let target = e.currentTarget; //XMLHttpRequest

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            this.afficheInfoFilm(data);


            // console.log(data[2].title);
            // console.log(data[2].release_date);
        }

    }

    afficheInfoFilm(data){

        console.log(data);

        document.querySelector("h1").innerText = data.title;

        let uneImage = document.querySelector("img").setAttribute("src", this.imgPath + "w" + this.largeurAffiche[5] + data.poster_path);

    }

    requeteInfoActeur(idFilm){



    }


}